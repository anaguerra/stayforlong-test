<?php declare(strict_types=1);

namespace Tests\Domain\FakeBuilder;

use App\Domain\Models\Booking;
use DateTime;
use DateTimeInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tests\FakeBuilder;

class BookingFakeBuilder extends FakeBuilder
{
    private UuidInterface $uuid;
    private string $bookingRequestId;
    private int $nights;
    private DateTimeInterface $checkIn;
    private int $sellingRate;
    private int $margin;


    public function random(): self
    {
        $this->uuid = Uuid::fromString($this->getFaker()->uuid);
        $this->bookingRequestId = $this->getFaker()->word;
        $this->nights = $this->getFaker()->randomNumber(1);
        $this->checkIn = new DateTime($this->getFaker()->date());
        $this->sellingRate = $this->getFaker()->randomNumber(3);
        $this->margin = $this->getFaker()->randomNumber(2);
        return $this;
    }

    public function withNights(int $nights): self
    {
        $this->nights = $nights;
        return $this;
    }

    public function withCheckIn(DateTimeInterface $checkIn): self
    {
        $this->checkIn = $checkIn;
        return $this;
    }

    public function withSellingRate(int $sellingRate): self
    {
        $this->sellingRate = $sellingRate;
        return $this;
    }

    public function withMargin(int $margin): self
    {
        $this->margin = $margin;
        return $this;
    }

    public function withRequestId(string $requestId): self
    {
        $this->bookingRequestId = $requestId;
        return $this;
    }

    public function generate(): Booking
    {
        return new Booking(
            $this->uuid,
            $this->bookingRequestId,
            $this->checkIn,
            $this->nights,
            $this->sellingRate,
            $this->margin
        );

    }


}
