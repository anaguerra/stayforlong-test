<?php declare(strict_types=1);

namespace Tests\Domain\Collections;

use App\Domain\Collections\BookingCollection;
use DateTime;
use Tests\Domain\FakeBuilder\BookingFakeBuilder;
use Tests\TestCase;

class BookingCollectionTest extends TestCase
{
    private BookingFakeBuilder $bookingFakeBuilder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bookingFakeBuilder = $this->app->get(BookingFakeBuilder::class);
    }

    public function testGetBookingMaximumAndMinimumAverageProfitPerNight(): void
    {
        $booking1 = $this->bookingFakeBuilder->withNights(5)
            ->withMargin(20)
            ->withSellingRate(200)
            ->generate();

        $booking2 = $this->bookingFakeBuilder->withNights(4)
            ->withMargin(22)
            ->withSellingRate(156)
            ->generate();

        $bookingCollection = new BookingCollection([$booking1, $booking2]);
        $bookingGreatestAvg = $bookingCollection->getBookingMaximumAverageProfitPerNight();
        $bookingLowestAvg = $bookingCollection->getBookingMinimumAverageProfitPerNight();

        $this->assertEquals($booking2, $bookingGreatestAvg);
        $this->assertEquals(8.58, $bookingGreatestAvg->getProfitPerNight());

        $this->assertEquals($booking1, $bookingLowestAvg);
        $this->assertEquals(8, $bookingLowestAvg->getProfitPerNight());
    }

    public function testProfitPerNightAverage(): void
    {
        $booking1 = $this->bookingFakeBuilder->withNights(1)
            ->withSellingRate(50)
            ->withMargin(20)
            ->generate();

        $booking2 = $this->bookingFakeBuilder->withNights(1)
            ->withSellingRate(55)
            ->withMargin(22)
            ->generate();

        $booking3 = $this->bookingFakeBuilder->withNights(1)
            ->withSellingRate(49)
            ->withMargin(21)
            ->generate();

        $bookingCollection = new BookingCollection([$booking1, $booking2, $booking3]);

        $this->assertEquals(10.80, $bookingCollection->getAverageProfitPerNight());
    }

    public function testGetBookingMaximumProfit(): void
    {
        $booking1 = $this->bookingFakeBuilder
            ->withSellingRate(1000)
            ->withMargin(10)
            ->generate();

        $booking2 = $this->bookingFakeBuilder
            ->withSellingRate(700)
            ->withMargin(10)
            ->generate();

        $booking3 = $this->bookingFakeBuilder
            ->withSellingRate(400)
            ->withMargin(10)
            ->generate();

        $bookingCollection = new BookingCollection([$booking1, $booking2, $booking3]);

        $this->assertEquals($booking1, $bookingCollection->getBookingWithMaximumProfit());
    }

    public function testBookingOverlapsWithExistingInBookingCollection(): void
    {
        $booking1 = $this->bookingFakeBuilder
            ->withCheckIn(new DateTime('2018-01-01'))
            ->withNights(10)
            ->withSellingRate(1000)
            ->withMargin(10)
            ->generate();

        $booking2 = $this->bookingFakeBuilder
            ->withCheckIn(new DateTime('2018-01-06'))
            ->withNights(10)
            ->withSellingRate(700)
            ->withMargin(10)
            ->generate();

        $booking3 = $this->bookingFakeBuilder
            ->withCheckIn(new DateTime('2018-01-12'))
            ->withNights(10)
            ->withSellingRate(400)
            ->withMargin(10)
            ->generate();

        $bookingCollection = new BookingCollection([$booking1, $booking3]);
        $this->assertTrue($bookingCollection->bookingOverlapsWithExistingInBookingCollection($booking2, $bookingCollection));
    }

    public function testMaximizeTotalProfits(): void
    {
        $booking1 = $this->bookingFakeBuilder
            ->withRequestId('bookata_XY123')
            ->withCheckIn(new DateTime('2020-01-01'))
            ->withNights(5)
            ->withSellingRate(200)
            ->withMargin(20)
            ->generate();

        $booking2 = $this->bookingFakeBuilder
            ->withRequestId('kayete_PP234')
            ->withCheckIn(new DateTime('2020-01-04'))
            ->withNights(4)
            ->withSellingRate(156)
            ->withMargin(5)
            ->generate();

        $booking3 = $this->bookingFakeBuilder
            ->withRequestId('atropote_AA930')
            ->withCheckIn(new DateTime('2020-01-04'))
            ->withNights(4)
            ->withSellingRate(150)
            ->withMargin(6)
            ->generate();

        $booking4 = $this->bookingFakeBuilder
            ->withRequestId('acme_AAAAA')
            ->withCheckIn(new DateTime('2020-01-10'))
            ->withNights(4)
            ->withSellingRate(160)
            ->withMargin(30)
            ->generate();

        $bookingCollection = new BookingCollection([$booking1, $booking2, $booking3, $booking4]);

        $bookingCollectionTotal = $bookingCollection->maximizeTotalProfits();

        $this->assertEquals(new BookingCollection([$booking4, $booking1]), $bookingCollectionTotal);

        $this->assertEquals(88, $bookingCollectionTotal->getTotalProfit());
        $this->assertEquals(10, $bookingCollectionTotal->getAverageProfitPerNight());
        $this->assertEquals(8, $bookingCollectionTotal->getBookingMinimumAverageProfitPerNight()->getProfitPerNight());
        $this->assertEquals(12, $bookingCollectionTotal->getBookingMaximumAverageProfitPerNight()->getProfitPerNight());
    }
}
