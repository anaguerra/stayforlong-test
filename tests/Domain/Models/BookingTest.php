<?php declare(strict_types=1);

namespace Tests\Domain\Models;

use App\Domain\Models\Booking;
use App\Domain\Models\DateRange;
use DateTime;
use Tests\Domain\FakeBuilder\BookingFakeBuilder;
use Tests\TestCase;

class BookingTest extends TestCase
{
    private BookingFakeBuilder $bookingFakeBuilder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bookingFakeBuilder = $this->app->get(BookingFakeBuilder::class);
    }

    public function testCreate(): void
    {
        $booking = $this->bookingFakeBuilder->generate();
        $this->assertInstanceOf(Booking::class, $booking);
    }

    public function profitPerNightDataProvider(): array
    {
        return [
            [5, 200, 20, 8],
            [4, 156, 22, 8.58]
        ];
    }

    /**
     * @dataProvider profitPerNightDataProvider
     */
    public function testProfitPerNight(
        int $nights,
        int $sellingRate,
        int $margin,
        float $expectedProfitPerNight
    ): void
    {
        $booking = $this->bookingFakeBuilder->withNights($nights)
            ->withMargin($margin)
            ->withSellingRate($sellingRate)
            ->generate();

        $this->assertEquals($expectedProfitPerNight, $booking->getProfitPerNight());
    }

    public function profitDataProvider(): array
    {
        return [
            [1000, 10, 100],
            [700, 10, 70],
            [400, 10, 40],
        ];
    }


    /**
     * @dataProvider profitDataProvider
     */
    public function testProfit(
        int $sellingRate,
        int $margin,
        float $expectedProfit
    ): void
    {
        $booking = $this->bookingFakeBuilder
            ->withMargin($margin)
            ->withSellingRate($sellingRate)
            ->generate();

        $this->assertEquals($expectedProfit, $booking->getProfit());
    }

    public function testDates(): void
    {
        $checkIn = new DateTime('2022-01-01');
        $booking = $this->bookingFakeBuilder
            ->withCheckIn($checkIn)
            ->withNights(3)
            ->generate();

        $expectedRange = new DateRange($checkIn, new DateTime('2022-01-04'));
        $this->assertEquals($expectedRange, $booking->getDateRange());
    }
}
