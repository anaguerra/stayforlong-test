<?php declare(strict_types=1);

namespace Tests\Domain\Models;

use App\Domain\Models\DateRange;
use DateTime;
use DateTimeInterface;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DateRangeTest extends TestCase
{
    public function testCreate()
    {
        $dateRange = new DateRange(new DateTime('2022-04-01'), new DateTime('2022-04-15'));
        $this->assertInstanceOf(DateRange::class, $dateRange);
        $this->assertEquals('2022-04-01', $dateRange->getStartDate()->format('Y-m-d'));
        $this->assertEquals('2022-04-15', $dateRange->getEndDate()->format('Y-m-d'));
    }


    public function testEquals()
    {
        $dateRange = new DateRange(new DateTime('2022-04-01'), new DateTime('2022-04-15'));
        $dateRange2 = new DateRange(new DateTime('2022-04-01'), new DateTime('2022-04-15'));
        $dateRange3 = new DateRange(new DateTime('2022-04-01'), new DateTime('2022-04-02'));

        $this->assertTrue($dateRange->equals($dateRange2));
        $this->assertFalse($dateRange->equals($dateRange3));
    }

    public function testCreateInvalidThrowException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('End Date "2022-04-01" must be greater than Start Date "2022-04-15"');
        $dateRange = new DateRange(new DateTime('2022-04-15'), new DateTime('2022-04-01'));
    }

    public function overlapDataProvider(): array
    {
        // dateStartRange1, dateEndRange1, dateStartRange2, dateEndRange2, areOverlapped
        return [
            ['2022-04-01', '2022-04-15', '2022-04-10', '2022-04-20', true],
            ['2022-04-01', '2022-04-15', '2022-04-15', '2022-04-20', true],
            ['2020-12-15', '2021-01-10', '2021-01-01', '2021-01-30', true],
            ['2022-04-01', '2022-04-15', '2022-04-16', '2022-04-20', false],
            ['2022-01-01', '2022-01-23', '2021-01-01', '2021-01-20', false],
            ['2021-01-01', '2021-01-20', '2022-01-01', '2022-01-23', false],
            ['2022-04-01', '2022-04-15', '2022-04-15', '2022-04-20', true],
        ];
    }

    /**
     * @dataProvider overlapDataProvider
     */
    public function testOverlapDateRange(
        string $dateRange1Start,
        string $dateRange1End,
        string $dateRange2Start,
        string $dateRange2End,
        bool   $overlap): void
    {
        $dateRange1 = new DateRange(new DateTime($dateRange1Start), new DateTime($dateRange1End));
        $dateRange2 = new DateRange(new DateTime($dateRange2Start), new DateTime($dateRange2End));
        $this->assertEquals($overlap, $dateRange1->overlapsWithDateRange($dateRange2));
    }


    public function dateIsInRangeDataProvider(): array
    {
        // dateStart, dateEnd, date, dateIsInRange
        return [
            ['2022-04-01', '2022-04-15', new DateTime('2022-04-04'), true],
            ['2021-12-15', '2022-01-10', new DateTime('2022-04-04'), false],
            ['2021-12-15', '2022-01-10', new DateTime('2021-12-15'), true],
            ['2020-12-15', '2021-01-10', new DateTime('2021-12-15'), false],
        ];
    }

    /**
     * @dataProvider dateIsInRangeDataProvider
     */
    public function testDateIsInRange(
        string            $dateRangeStart,
        string            $dateRangeEnd,
        DateTimeInterface $date,
        bool              $dateIsInRange
    ): void
    {
        $dateRange = new DateRange(new DateTime($dateRangeStart), new DateTime($dateRangeEnd));
        $this->assertEquals($dateIsInRange, $dateRange->dateIsInRange($date));
    }
}
