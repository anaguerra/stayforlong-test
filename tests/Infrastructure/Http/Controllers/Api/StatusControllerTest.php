<?php declare(strict_types=1);

namespace Tests\Infrastructure\Http\Controllers\Api;

use Illuminate\Http\Response;
use Tests\TestCase;

class StatusControllerTest extends TestCase
{

    private const API_STATUS_URL = '/api/status';

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testGetStatusReturns200()
    {
        $response = $this->json('GET', self::API_STATUS_URL);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(['message' => 'OK']);
    }
}
