<?php declare(strict_types=1);

namespace Tests\Infrastructure\Http\Controllers\Api;

use App\Infrastructure\Http\Controllers\Api\Stats\StatsController;
use Tests\TestCase;

class PostStatsTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }


    public function testStatsAction() : void
    {
        $response = $this->post(action([StatsController::class, 'stats']), $this->getRequestData());

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'avg_night' => 10.8,
                'min_night' => 10,
                'max_night' => 12.1,
            ],
        ]);
    }

    private function getRequestData(): array
    {
        return[
            [
                'request_id' => 'bookata_XY123',
                'nights' => 1,
                'check_in' => '2020-01-01',
                'selling_rate' => 50,
                'margin' => 20,
            ],
             [
                'request_id' => 'kayete_PP234',
                'nights' => 1,
                'check_in' => '2020-01-04',
                'selling_rate' => 55,
                'margin' => 22,
            ],
             [
                'request_id' => 'trivoltio_ZX69',
                'nights' => 1,
                'check_in' => '2020-01-07',
                'selling_rate' => 49,
                'margin' => 21,
            ]
        ];
    }
}
