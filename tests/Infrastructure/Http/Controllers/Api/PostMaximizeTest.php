<?php declare(strict_types=1);

namespace Tests\Infrastructure\Http\Controllers\Api;

use App\Infrastructure\Http\Controllers\Api\Maximize\MaximizeController;
use Tests\TestCase;

class PostMaximizeTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }


    public function testMaximizeAction() : void
    {
        $response = $this->post(action([MaximizeController::class, '__invoke']), $this->getRequestData());

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'request_ids' => [
                    'acme_AAAAA',
                    'bookata_XY123'
                ],
                'total_profit' => 88,
                'avg_night' => 10,
                'min_night' => 8,
                'max_night' => 12
            ],
        ]);
    }

    private function getRequestData(): array
    {
        return[
            [
                'request_id' => 'bookata_XY123',
                'nights' => 5,
                'check_in' => '2020-01-01',
                'selling_rate' => 200,
                'margin' => 20,
            ],
             [
                'request_id' => 'kayete_PP234',
                'nights' => 4,
                'check_in' => '2020-01-04',
                'selling_rate' => 156,
                'margin' => 5,
            ],
             [
                'request_id' => 'atropote_AA930',
                'nights' => 4,
                'check_in' => '2020-01-04',
                'selling_rate' => 150,
                'margin' => 6,
            ],
             [
                'request_id' => 'acme_AAAAA',
                'nights' => 4,
                'check_in' => '2020-01-10',
                'selling_rate' => 160,
                'margin' => 30,
            ]
        ];
    }
}
