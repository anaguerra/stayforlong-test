<?php declare(strict_types=1);

namespace Tests\Application\Command;

use App\Application\Command\CalculateBookingStats;
use App\Application\Command\CalculateBookingStatsHandler;
use App\Domain\Collections\BookingCollection;
use App\Domain\Models\Stats;
use Tests\Domain\FakeBuilder\BookingFakeBuilder;
use Tests\TestCase;

class CalculateBookingStatsHandlerTest extends TestCase
{
    private CalculateBookingStatsHandler $handler;
    private BookingFakeBuilder $bookingFakeBuilder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->handler =  $this->app->get(CalculateBookingStatsHandler::class);
        $this->bookingFakeBuilder = $this->app->get(BookingFakeBuilder::class);
    }

    public function testHandle(): void
    {
        $expectedStats = new Stats(8.29, 8, 8.58);
        $command = new CalculateBookingStats($this->getBookings());
        $this->assertEquals($expectedStats, $this->handler->handle($command));
    }

    private function getBookings(): BookingCollection
    {
        $booking1 = $this->bookingFakeBuilder->withRequestId('bookata_XY123')
            ->withNights(5)
            ->withCheckIn(new \DateTime('2020-01-01'))
            ->withSellingRate(200)
            ->withMargin(20)
            ->generate();
        $booking2 = $this->bookingFakeBuilder->withRequestId('kayete_PP234')
            ->withNights(4)
            ->withCheckIn(new \DateTime('2020-01-04'))
            ->withSellingRate(156)
            ->withMargin(22)
            ->generate();
        return new BookingCollection([$booking1, $booking2]);
    }
}
