# Stayforlong Test

## Summary

- I've used PHP, and I've used Laravel framework. 
- I've tried to apply Hexagonal Architecture and SOLID principles.
- There are several tests but I've not done all that should be required.
- Some improvements could be done (and more tests) but for the purposes of the test I think that is enough so you can see how I code.

## Installation and execution

1) `make start`
2) `make prepare`
3) `make run` 
4) To run the tests: `make run-tests`
5) Call endpoints

**/stats**

```bash
curl --location --request POST 'localhost:8000/api/stats' \
--header 'Content-Type: application/json' \
--data-raw '[
    {
        "request_id": "bookata_XY123",
        "check_in": "2020-01-01",
        "nights": 1,
        "selling_rate": 50,
        "margin": 20
    },
    {
        "request_id": "kayete_PP234",
        "check_in": "2020-01-04",
        "nights": 1,
        "selling_rate": 55,
        "margin": 22
    },
    {
        "request_id": "trivoltio_ZX69",
        "check_in": "2020-01-07",
        "nights": 1,
        "selling_rate": 49,
        "margin": 21
    }
]'
```

**/mazimize**

```bash

curl --location --request POST 'localhost:8000/api/maximize' \
--header 'Content-Type: application/json' \
--data-raw '[
    {
        "request_id": "bookata_XY123",
        "check_in": "2020-01-01",
        "nights": 5,
        "selling_rate": 200,
        "margin": 20
    },
    {
        "request_id": "kayete_PP234",
        "check_in": "2020-01-04",
        "nights": 4,
        "selling_rate": 156,
        "margin": 5
    },
    {
        "request_id": "atropote_AA930",
        "check_in": "2020-01-04",
        "nights": 4,
        "selling_rate": 150,
        "margin": 6
    },
    {
        "request_id": "acme_AAAAA",
        "check_in": "2020-01-10",
        "nights": 4,
        "selling_rate": 160,
        "margin": 30
    }
]'

```


