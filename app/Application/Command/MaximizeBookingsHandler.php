<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Models\MaximizedBookings;

class MaximizeBookingsHandler
{
    public function handle(MaximizeBookings $command): MaximizedBookings
    {
        $bookingCollection = $command->getBookings();
        $bookingsMaximizeProfits = $bookingCollection->maximizeTotalProfits();

        return new MaximizedBookings(
            $bookingsMaximizeProfits->getRequestIds(),
            $bookingsMaximizeProfits->getTotalProfit(),
            $bookingsMaximizeProfits->getAverageProfitPerNight(),
            $bookingsMaximizeProfits->getBookingMinimumAverageProfitPerNight()->getProfitPerNight(),
            $bookingsMaximizeProfits->getBookingMaximumAverageProfitPerNight()->getProfitPerNight(),
        );
    }
}
