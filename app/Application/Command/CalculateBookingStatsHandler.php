<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Models\Stats;

class CalculateBookingStatsHandler
{
    public function handle(CalculateBookingStats $command): Stats
    {
        $bookingCollection = $command->getBookings();

        $bookingMaxProfitNight = $bookingCollection->getBookingMaximumAverageProfitPerNight();
        $bookingMinProfitNight = $bookingCollection->getBookingMinimumAverageProfitPerNight();
        $bookingAverageNight = $bookingCollection->getAverageProfitPerNight();

        return new Stats(
            $bookingAverageNight,
            $bookingMinProfitNight->getProfitPerNight(),
            $bookingMaxProfitNight->getProfitPerNight(),
        );
    }
}
