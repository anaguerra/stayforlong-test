<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Collections\BookingCollection;

class MaximizeBookings
{
    private BookingCollection $bookings;

    public function __construct(BookingCollection $bookings)
    {
        $this->bookings = $bookings;
    }

    public function getBookings(): BookingCollection
    {
        return $this->bookings;
    }
}
