<?php

declare(strict_types=1);

namespace App\Application\Exception;

use Throwable;

class ValidationException extends \InvalidArgumentException implements ApplicationException
{
    private const CODE = 400;
    private array $errors;

    public function __construct(string $message = '', array $errors = [], Throwable $previous = null)
    {
        parent::__construct($message, self::CODE, $previous);
        $this->errors = $errors;
    }

    public function getValidationErrors(): array
    {
        return $this->errors;
    }
}
