<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use Exception;

class BookingCollectionException extends Exception implements DomainException
{
    public static function collectionIsEmpty(string $message): self
    {
        return new self(sprintf('The collection is empty. %s', $message));
    }
}
