<?php

declare(strict_types=1);

namespace App\Domain\Collections;

use App\Domain\Exception\BookingCollectionException;
use App\Domain\Models\Booking;

class BookingCollection extends ObjectCollection
{
    /**
     * @return Booking[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public static function allowedObjectClass(): string
    {
        return Booking::class;
    }

    protected function itemAssertions($item): void
    {
    }

    public function getBookingMaximumAverageProfitPerNight(): Booking
    {
        if ($this->isEmpty()) {
            throw BookingCollectionException::collectionIsEmpty('Method getBookingMaximumAverageProfitPerNight required at least 1 Booking in the collection');
        }

        $bookings = $this->getItems();
        $hasGreaterAvg = array_pop($bookings);

        return array_reduce(
            $bookings,
            fn (Booking $hasGreaterAvg, Booking $item) => $hasGreaterAvg->getProfitPerNight() > $item->getProfitPerNight()
                ? $hasGreaterAvg
                : $item,
            $hasGreaterAvg
        );
    }

    public function getBookingMinimumAverageProfitPerNight(): Booking
    {
        if ($this->isEmpty()) {
            throw BookingCollectionException::collectionIsEmpty('Method getBookingMinimumAverageProfitPerNight required at least 1 Booking in the collection');
        }

        $bookings = $this->getItems();
        $hasLowestAvg = array_pop($bookings);

        return array_reduce(
            $bookings,
            fn (Booking $hasLowestAvg, Booking $item) => $hasLowestAvg->getProfitPerNight() < $item->getProfitPerNight()
                ? $hasLowestAvg
                : $item,
            $hasLowestAvg
        );
    }

    public function getAverageProfitPerNight(): float
    {
        $profits = array_map(fn (Booking $booking) => $booking->getProfitPerNight(), $this->items);

        $avg = array_sum($profits) / $this->count();

        return ceil($avg * 100) / 100;
    }

    public function getBookingWithMaximumProfit(): Booking
    {
        $bookings = $this->getItems();
        $hasMaxProfit = array_pop($bookings);

        return array_reduce(
            $bookings,
            fn (Booking $hasMaxProfit, Booking $item) => $hasMaxProfit->getProfit() > $item->getProfit()
                ? $hasMaxProfit
                : $item,
            $hasMaxProfit
        );
    }

    public function maximizeTotalProfits(): self
    {
        $bookingFirst = $this->getBookingWithMaximumProfit();
        $combinationCollection = new self();
        $combinationCollection->add($bookingFirst);

        foreach ($this->items as $booking) {
            /** @var Booking $booking */
            if (false === $this->bookingOverlapsWithExistingInBookingCollection($booking, $combinationCollection)) {
                $combinationCollection->add($booking);
            }
        }

        return $combinationCollection;
    }

    public function bookingOverlapsWithExistingInBookingCollection(Booking $item, BookingCollection $bookingCollection): bool
    {
        foreach ($bookingCollection->items as $booking) {
            /** @var Booking $booking */
            if ($item->getDateRange()->overlapsWithDateRange($booking->getDateRange())) {
                return true;
            }
        }
        return false;
    }

    public function getTotalProfit(): float
    {
        $totalProfit = 0;
        foreach ($this->items as $booking) {
            $totalProfit += $booking->getProfit();
        }
        return $totalProfit;
    }

    public function getRequestIds(): array
    {
        return array_map(fn (Booking $booking) => $booking->getBookingRequestId(), $this->items);
    }
}
