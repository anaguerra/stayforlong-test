<?php

declare(strict_types=1);

namespace App\Domain\Services;

use Ramsey\Uuid\UuidInterface;

interface UuidFactory
{
    public function generate(): UuidInterface;
}
