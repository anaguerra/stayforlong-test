<?php

declare(strict_types=1);

namespace App\Domain\Models;

use DateTime;
use DateTimeInterface;
use InvalidArgumentException;

class DateRange
{
    private DateTimeInterface $startDate;
    private DateTimeInterface $endDate;

    public function __construct(DateTimeInterface $startDate, DateTimeInterface $endDate)
    {
        $this->assertDates($startDate, $endDate);
        $this->startDate = new DateTime($startDate->format('Y-m-d'));
        $this->endDate = new DateTime($endDate->format('Y-m-d'));
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }

    public function overlapsWithDateRange(DateRange $dateRange): bool
    {
        return $this->startDate <= $dateRange->endDate
                && $this->endDate >= $dateRange->startDate;
    }

    public function dateIsInRange(DateTimeInterface $date): bool
    {
        return $date <= $this->endDate && $date >= $this->startDate;
    }

    public function equals(DateRange $dateRange): bool
    {
        return $this->startDate->diff($dateRange->startDate)->days === 0
            && $this->endDate->diff($dateRange->endDate)->days === 0;
    }

    private function assertDates(DateTimeInterface $startDate, DateTimeInterface $endDate): void
    {
        if ($endDate < $startDate) {
            throw new InvalidArgumentException(
                sprintf(
                    'End Date "%s" must be greater than Start Date "%s"',
                    $endDate->format('Y-m-d'),
                    $startDate->format('Y-m-d')
                )
            );
        }
    }
}
