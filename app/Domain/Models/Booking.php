<?php

declare(strict_types=1);

namespace App\Domain\Models;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

class Booking
{
    private UuidInterface $uuid;
    private string $bookingRequestId;
    private int $nights;
    private DateTimeInterface $checkIn;
    private int $sellingRate;
    private int $margin;

    public function __construct(
        UuidInterface $uuid,
        string $bookingRequestId,
        DateTimeInterface $checkIn,
        int $nights,
        int $sellingRate,
        int $margin
    ) {
        $this->uuid = $uuid;
        $this->bookingRequestId = $bookingRequestId;
        $this->checkIn = $checkIn;
        $this->nights = $nights;
        $this->sellingRate = $sellingRate;
        $this->margin = $margin;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getBookingRequestId(): string
    {
        return $this->bookingRequestId;
    }

    public function getCheckIn(): DateTimeInterface
    {
        return $this->checkIn;
    }

    public function getSellingRate(): int
    {
        return $this->sellingRate;
    }

    public function getMargin(): int
    {
        return $this->margin;
    }

    public function getProfitPerNight(): float
    {
        return ($this->sellingRate * ($this->margin/100)) / $this->nights;
    }

    public function getProfit(): float
    {
        return $this->sellingRate * ($this->margin/100);
    }

    public function getDateRange(): DateRange
    {
        $finish = (clone $this->checkIn)->add(new DateInterval('P'. $this->nights .'D'));

        return new DateRange($this->checkIn, $finish);
    }
}
