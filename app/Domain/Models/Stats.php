<?php

declare(strict_types=1);

namespace App\Domain\Models;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

class Stats
{
    private float $avgNight;
    private float $minNight;
    private float $maxNight;

    public function __construct(float $avgNight, float $minNight, float $maxNight)
    {
        $this->avgNight = $avgNight;
        $this->minNight = $minNight;
        $this->maxNight = $maxNight;
    }

    public function getAvgNight(): float
    {
        return $this->avgNight;
    }

    public function getMinNight(): float
    {
        return $this->minNight;
    }

    public function getMaxNight(): float
    {
        return $this->maxNight;
    }

    public function toArray(): array
    {
        return [
            'avg_night' => $this->avgNight,
            'min_night' => $this->minNight,
            'max_night' => $this->maxNight,
        ];
    }
}
