<?php

declare(strict_types=1);

namespace App\Domain\Models;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

class MaximizedBookings
{
    private array $requestIds; // TODO: valueObject Collection
    private float $totalProfit;
    private float $avgNight;
    private float $minNight;
    private float $maxNight;

    public function __construct(array $requestIds, float $totalProfit, float $avgNight, float $minNight, float $maxNight)
    {
        $this->requestIds = $requestIds;
        $this->totalProfit = $totalProfit;
        $this->avgNight = $avgNight;
        $this->minNight = $minNight;
        $this->maxNight = $maxNight;
    }

    public function getAvgNight(): float
    {
        return $this->avgNight;
    }

    public function getMinNight(): float
    {
        return $this->minNight;
    }

    public function getMaxNight(): float
    {
        return $this->maxNight;
    }

    public function getRequestIds(): array
    {
        return $this->requestIds;
    }

    public function getTotalProfit(): float
    {
        return $this->totalProfit;
    }

    public function toArray(): array
    {
        return [
            'request_ids' => $this->requestIds,
            'total_profit' => $this->totalProfit,
            'avg_night' => $this->avgNight,
            'min_night' => $this->minNight,
            'max_night' => $this->maxNight,
        ];
    }
}
