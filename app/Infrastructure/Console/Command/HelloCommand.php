<?php

declare(strict_types=1);

namespace App\Infrastructure\Console\Command;

use Illuminate\Console\Command;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Psr\Log\LoggerInterface;

class HelloCommand extends Command
{
    /** @var string */
    protected $signature = 'hello {name : A name}';
    /** @var string */
    protected $description = 'Say hello';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ExceptionHandler $exceptionHandler, LoggerInterface $logger): int
    {
        try {
            $name = $this->argument('name');
            $this->output->success(sprintf('Hello %s!', $name));
            return 0;
        } catch (\Throwable $exception) {
            $message = sprintf('Problem saying hello: %s', $exception->getMessage());
            $this->output->error($message);
            $logger->critical($message);
            /** @noinspection PhpUnhandledExceptionInspection */
            $exceptionHandler->report($exception);
            return 1;
        }
    }
}
