<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Requests;

use App\Domain\Collections\BookingCollection;
use App\Domain\Models\Booking;
use DateTime;
use Illuminate\Foundation\Http\FormRequest;
use Ramsey\Uuid\Uuid;

class StatsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            '*' => 'required|array',
            '*request_id' => 'string|required',
            '*check_in' => 'string|required',
            '*nights' => 'string|required',
            '*selling_rate' => 'string|required',
            '*margins' => 'string|required',
        ];
    }

    public function getBookings(): BookingCollection
    {
        return new BookingCollection(array_map(
            fn ($bookingArr) => new Booking(
                Uuid::uuid4(),
                $bookingArr['request_id'],
                new DateTime($bookingArr['check_in']),
                (int)$bookingArr['nights'],
                (int)$bookingArr['selling_rate'],
                (int)$bookingArr['margin'],
            ),
            $this->validated()
        ));
    }
}
