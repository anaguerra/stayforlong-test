<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api;

use App\Application\Exception\ValidationException;
use App\Infrastructure\Http\Controllers\Controller;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Api Test Stayforlong",
 *      @OA\Contact(email="devsupport@test.com"
 *      ),
 * ),
 * @OA\Schema(
 *      schema="ErrorResponse",
 *      required={"message", "errors"},
 *      @OA\Property(
 *          property="message",
 *          type="string",
 *          example="A message"
 *      ),
 *     @OA\Property(
 *          property="errors",
 *          @OA\Property(property="errorKey", type="array", @OA\Items(type="string", example="Error description."))
 *     ),
 * ),
 */
class ApiController extends Controller
{
}
