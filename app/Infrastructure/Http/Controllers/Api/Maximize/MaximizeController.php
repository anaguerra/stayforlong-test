<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api\Maximize;

use App\Application\Command\MaximizeBookings;
use App\Application\Command\MaximizeBookingsHandler;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Http\Requests\MaximizeRequest;
use App\Infrastructure\Http\Resources\MaximizeBookingsResource;
use Illuminate\Http\Response;

class MaximizeController extends Controller
{
    public function __invoke(MaximizeRequest $request): MaximizeBookingsResource
    {
        $maximizedBookings = (new MaximizeBookingsHandler())->handle(new MaximizeBookings($request->getBookings()));

        return new MaximizeBookingsResource($maximizedBookings, Response::HTTP_OK);
    }
}
