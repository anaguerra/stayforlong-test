<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api\Stats;

use App\Application\Command\CalculateBookingStats;
use App\Application\Command\CalculateBookingStatsHandler;
use App\Infrastructure\Http\Controller;
use App\Infrastructure\Http\Requests\StatsRequest;
use App\Infrastructure\Http\Resources\StatsResource;
use Illuminate\Http\Response;

/**
 * @OA\Post(
 *     path="/api/stats",
 *     tags={"Stats"},
 *     @OA\RequestBody(
 *         description="booking stats",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(ref="#/components/schemas/StatsRequestData"),
 *         )
 *     ),
 *      @OA\Response(
 *          response=201,
 *          description="Successful operation",
 *          @OA\JsonContent(ref="#/components/schemas/StatsResource"),
 *     ),
 *     @OA\Response(
 *          response=400,
 *          description="Bad Request",
 *          @OA\JsonContent(ref="#/components/schemas/ErrorResponse"),
 *       ),
 *     @OA\Response(
 *          response=500,
 *          description="server error",
 *       ),
 * )
 *
 *
 *
 * @OA\Schema(
 *     schema="StatsRequestData",
 *     @OA\Property(type="array", @OA\Items(ref="#/components/schemas/BookingFormData")),
 * )
 *
 * @OA\Schema(
 *      schema="BookingFormData",
 *      required={"booking_request_id", "nights", "check_in", "selling_rate", "margin"},
 *      @OA\Property(property="booking_request_id", type="string"),
 *      @OA\Property(property="nights", type="string"),
 *      @OA\Property(property="check_in", type="string"),
 *      @OA\Property(property="selling_rate", type="string"),
 *      @OA\Property(property="margin", type="string"),
 * )
 */
class StatsController extends Controller
{
    public function stats(StatsRequest $request): StatsResource
    {
        $stats = (new CalculateBookingStatsHandler())->handle(new CalculateBookingStats($request->getBookings()));

        return new StatsResource($stats, Response::HTTP_OK);
    }
}
