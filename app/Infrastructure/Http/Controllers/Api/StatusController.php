<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenApi\Annotations as OA;

class StatusController extends ApiController
{
    /**
     * @OA\Get(
     *      path="/api/status",
     *      tags={"Status"},
     *      summary="Get service status",
     *      description="Returns the service status",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json"
     *         )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(ref="#/components/schemas/EmtyResponse")
     *      )
     *   )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function status(Request $request): JsonResponse
    {
        $responseContents = ['message' => 'OK'];
        $responseContents['data'] = [];
        return new JsonResponse($responseContents, Response::HTTP_OK);
    }
}
