<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resources;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class JsonApiResource extends JsonResource
{
    protected ?int $statusCode;

    /** @param mixed $resource */
    public function __construct($resource, int $statusCode = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($resource);
    }

    public function toResponse($request): JsonResponse
    {
        $response =  parent::toResponse($request);
        if ($this->statusCode) {
            $response->setStatusCode($this->statusCode);
        }
        return $response;
    }
}
