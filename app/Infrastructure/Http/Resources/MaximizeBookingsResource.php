<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resources;

use App\Domain\Models\MaximizedBookings;

class MaximizeBookingsResource extends JsonApiResource
{
    public function __construct(MaximizedBookings $resource, int $statusCode = null)
    {
        parent::__construct($resource, $statusCode);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request): array
    {
        return $this->resource->toArray();
    }
}
