<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Resources;

use App\Domain\Models\Stats;

/**
 * @OA\Schema(
 *     @OA\Property(
 *          property="data",
 *          type="object",
 *          @OA\Property(property="avg_night", type="float", example="2.3"),
 *          @OA\Property(property="min_night", type="float", example="2.3"),
 *          @OA\Property(property="max_night", type="float", example="2.3"),
 *     )
 * )
 */
class StatsResource extends JsonApiResource
{
    public function __construct(Stats $resource, int $statusCode = null)
    {
        parent::__construct($resource, $statusCode);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     */
    public function toArray($request): array
    {
        return $this->resource->toArray();
    }
}
