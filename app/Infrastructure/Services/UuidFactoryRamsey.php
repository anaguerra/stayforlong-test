<?php

declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Domain\Services\UuidFactory;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidFactoryRamsey implements UuidFactory
{
    public function generate(): UuidInterface
    {
        return Uuid::uuid4();
    }
}
