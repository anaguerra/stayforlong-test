<?php

namespace App\Infrastructure\Providers;

use App\Domain\Services\UuidFactory;
use App\Infrastructure\Services\UuidFactoryRamsey;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UuidFactory::class, function () {
            return new UuidFactoryRamsey();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
