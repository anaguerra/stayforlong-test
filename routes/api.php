<?php

use App\Infrastructure\Http\Controllers\Api\Maximize\MaximizeController;
use App\Infrastructure\Http\Controllers\Api\Stats\StatsController;
use App\Infrastructure\Http\Controllers\Api\StatusController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('App\Infrastructure\Http\Controllers')
    ->group(function () {

        //Public Routes
        Route::middleware(['api'])
//            ->prefix('v1/')
            ->group(function () {
                Route::get('status', [StatusController::class, 'status'])
                    ->name('api.get.status');
                Route::post('stats', [StatsController::class, 'stats'])
                    ->name('api.post.stats');
                Route::post('maximize', [MaximizeController::class, '__invoke'])
                    ->name('api.post.maximize');
            });
});

